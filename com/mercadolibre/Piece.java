package com.mercadolibre;

import java.awt.image.BufferedImage;

public class Piece {
    Piece(){ }

    Piece(BufferedImage img) {
        this.img = img;
    }

    Piece(Integer upLeft, Integer upRight, Integer downLeft, Integer downRight) {
        this.upLeft = upLeft;
        this.upRight = upRight;
        this.downLeft = downLeft;
        this.downRight = downRight;
    }

    private BufferedImage img;
    private Integer upRight;
    private Integer upLeft;
    private Integer downRight;
    private Integer downLeft;

    public BufferedImage img() {
        return img;
    }

    public void img(BufferedImage img) {
        this.img = img;
    }

    public Integer upRight() {
        return upRight;
    }

    public void upRight(Integer upRight) {
        this.upRight = upRight;
    }

    public Integer upLeft() {
        return upLeft;
    }

    public void upLeft(Integer upLeft) {
        this.upLeft = upLeft;
    }

    public Integer downRight() {
        return downRight;
    }

    public void downRight(Integer downRight) {
        this.downRight = downRight;
    }

    public Integer downLeft() {
        return downLeft;
    }

    public void downLeft(Integer downLeft) {
        this.downLeft = downLeft;
    }
}
