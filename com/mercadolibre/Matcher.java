package com.mercadolibre;

public class Matcher {

    public static boolean pieceMatchesPosition(Piece piece, Piece[][] matrix, int x, int y) {
        boolean isMatch = true;

        if (matrix[x][y-1] != null) {
            isMatch = isMatch && matchesVertically(matrix[x][y-1], piece);
        }

        if (matrix[x][y+1] != null) {
            isMatch = isMatch && matchesVertically(piece, matrix[x][y+1]);
        }

        if (matrix[x-1][y] != null) {
            isMatch = isMatch && matchesHorizontally(matrix[x-1][y], piece);
        }

        if (matrix[x+1][y] != null) {
            isMatch = isMatch && matchesHorizontally(piece, matrix[x+1][y]);
        }

        return isMatch;
    }

    private static boolean matchesHorizontally(Piece leftPiece, Piece rightPiece) {
        return leftPiece.upRight() == rightPiece.upLeft()
                && leftPiece.downRight() == rightPiece.downLeft();
    }

    private static boolean matchesVertically(Piece upperPiece, Piece lowerPiece) {
        return upperPiece.downLeft() == lowerPiece.upLeft()
                && upperPiece.downRight() == lowerPiece.upRight();
    }

}
