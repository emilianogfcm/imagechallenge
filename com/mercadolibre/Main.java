package com.mercadolibre;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {

        // Parse
        String imagePath = "./imgs/challenge.png";
        File file = new File(imagePath);
        FileInputStream fis = new FileInputStream(file);
        BufferedImage image = ImageIO.read(fis);

        int rows = 20;
        int cols = 20;

        int chunkWidth = image.getWidth() / cols;
        int chunkHeight = image.getHeight() / rows;

        Piece imgs[][] = new Piece[cols][rows];

        Map<Integer, Integer> rgbCount = new HashMap<>();
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                Piece piece = new Piece(new BufferedImage(chunkWidth -1, chunkHeight -1, image.getType()));

                SplitImage(image, piece.img(), chunkWidth, chunkHeight, x, y);

                piece.upLeft(getColor(2, 2, piece));
                piece.upRight(getColor(chunkWidth - 2, 2, piece));
                piece.downLeft(getColor(2, chunkHeight - 2, piece));
                piece.downRight(getColor(chunkHeight - 2, chunkWidth - 2, piece));

                rgbCount.putIfAbsent(piece.upLeft(), 0);
                rgbCount.putIfAbsent(piece.upRight(), 0);
                rgbCount.putIfAbsent(piece.downLeft(), 0);
                rgbCount.putIfAbsent(piece.downRight(), 0);

                rgbCount.put(piece.upLeft(), rgbCount.get(piece.upLeft()) + 1);
                rgbCount.put(piece.upRight(), rgbCount.get(piece.upRight()) + 1);
                rgbCount.put(piece.downLeft(), rgbCount.get(piece.downLeft()) + 1);
                rgbCount.put(piece.downRight(), rgbCount.get(piece.downRight()) + 1);

                imgs[x][y] = piece;
            }
        }

        System.out.println(rgbCount);

        // Sort

        Piece [][] orderedImage = initOuterFrame();
        for (int x=0; x<=19; x++){
            for (int y=0; y<=19; y++){
                if(Matcher.pieceMatchesPosition(imgs[x][y], orderedImage, 1,1)){
                    System.out.println("Match found for position 1,1: (" + x + ", " + y + ")");
                }

                if (Matcher.pieceMatchesPosition(imgs[x][y], orderedImage, 20,1)) {
                    System.out.println("Match found for position 20,1: (" + x + ", " + y + ")");
                }

                if (Matcher.pieceMatchesPosition(imgs[x][y], orderedImage, 1,20)) {
                    System.out.println("Match found for position 1,20: (" + x + ", " + y + ")");
                }

                if (Matcher.pieceMatchesPosition(imgs[x][y], orderedImage, 20,20)) {
                    System.out.println("Match found for position 20,20: (" + x + ", " + y + ")");
                }


            }
        }


        // Print

        BufferedImage result = new BufferedImage(image.getWidth() - 20, image.getHeight() - 20, image.getType());
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                ReuniteImage(imgs[x][y].img(), result, chunkWidth, chunkHeight, x, y);
            }
        }

        File outputfile = new File("./imgs/challenge_result.png");
        ImageIO.write(result, "png", outputfile);

    }

    public static void ReuniteImage(BufferedImage image, BufferedImage chunk, int chunkWidth, int chunkHeight, int x, int y) throws IOException {
        Graphics2D gr = chunk.createGraphics();
        gr.drawImage(image,
                chunkWidth * y - y, chunkHeight * x - x,
                chunkWidth * y + chunkWidth - y, chunkHeight * x + chunkHeight - x,
                0, 0,
                chunkWidth, chunkHeight,
                null);
        gr.dispose();
    }

    public static void SplitImage(BufferedImage image, BufferedImage chunk, int chunkWidth, int chunkHeight, int x, int y) throws IOException {
        Graphics2D gr = chunk.createGraphics();
        gr.drawImage(image,
                0, 0,
                chunkWidth-1, chunkHeight-1,
                chunkWidth * y +1, chunkHeight * x +1,
                chunkWidth * y + chunkWidth,
                chunkHeight * x + chunkHeight, null);
        gr.dispose();

        File outputfile = new File("./imgs/challenge_" + x + "_" + y + ".png");
        ImageIO.write(chunk, "png", outputfile);
    }


    private static Integer getColor(int x, int y, Piece piece) {
        if (Color.BLACK.getRGB() == (piece.img().getRGB(x, y)) || Color.WHITE.getRGB() == piece.img().getRGB(x, y)) {
            return null;
        }

        return piece.img().getRGB(x, y);
    }

    private static Piece[][] initOuterFrame(){
        Piece imgs[][] = new Piece[22][22];

        for (int x=0; x<=21; x++) {
            imgs[x][0] = new Piece(null, null, null, null);
            imgs[x][21] = new Piece(null, null, null, null);
        }

        for (int y=1; y<=20; y++) {
            imgs[0][y] = new Piece(null, null, null, null);
            imgs[21][y] = new Piece(null, null, null, null);
        }

        return imgs;
    }
}
